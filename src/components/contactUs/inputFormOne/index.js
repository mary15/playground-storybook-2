import React, {Component} from 'react';
import '../inputFormOne/style.css'

class InputForm extends Component {

    render(){

        const {id, placeholder} = this.props;
        
        return(
            <div>
                <form>
                    <input className = 'inputForm' id = {id} placeholder = {placeholder}></input>
                </form>
            </div>
        )
    }

}

export default InputForm;