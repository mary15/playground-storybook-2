import React, {Component} from 'react';
import InputForm from '../contactUs/inputFormTwo/index';
import TextArea from '../contactUs/textArea/index';
import SubmitButton from '../myButtons/colorButton/index';

class ContactUsForm extends Component {
    render(){
        return(
            <div>
                <form>
                    <div>
                        <InputForm id = 'firstName' placeholder = 'First Name' size = 'inputFormTwo'></InputForm>
                        <InputForm id = 'lastName' placeholder = 'Last Name' size = 'inputFormTwo'></InputForm>
                    </div>            
                    <InputForm id = 'email' placeholder = 'email address' size = 'inputFormTwoSize'></InputForm>
                    <TextArea id = 'message' placeholder = 'message...'></TextArea>
                    <SubmitButton id = 'colorButton' buttonText = 'Submit'></SubmitButton>

                </form>
            </div>
        )
    }
}

export default ContactUsForm;