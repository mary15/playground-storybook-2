import React, {Component} from 'react';
import "../textArea/style.css"

class MyTextArea extends Component {
    render(){
        const {id, placeholder} = this.props;
        return(
            <div>
                <form>
                    <textarea   className   = 'textAreaOne'
                                id          = {id}
                                placeholder = {placeholder}>
                    </textarea>
                </form>
            </div>
        )
    }
}

export default MyTextArea;