import React, {Component} from 'react';
import "../inputFormTwo/style.css"

class InputFormTwo extends Component {

    render(){
        const {id, placeholder, size} = this.props;
        return(
            <input className = {size} id = {id} placeholder = {placeholder}></input>
        )
    }

}

export default InputFormTwo;