import React, {Component} from 'react';
import '../cardTwo/style.css';
const sampleImage = require('../cardTwo/samplethree.png');

class CardTwo extends Component {
    render(){
        const {title, description} = this.props;
        return(
            <div className = "cardTwo">
                <img className="imgStyleTwo" src = {sampleImage}/>
                <div className = "containerTwo">
                    <h4 className="textTitle">{title}</h4>
                    <p className="textDescription">{description}</p>
                </div>
            </div>
        )
    }
}

export default CardTwo;