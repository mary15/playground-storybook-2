import React, {Component} from 'react';
import '../cardOne/style.css';

const blockchain = require('../cardOne/cardOne.png');

class CardOne extends Component {
    render(){
        const {title, description} = this.props;
        return(
            <div className = "card">
                <img className="imgStyle" src = {blockchain}/>
                <div className = "container">
                    <h4>{title}</h4>
                    <p>{description}</p>
                </div>
            </div>
        )
    }
}

export default CardOne;