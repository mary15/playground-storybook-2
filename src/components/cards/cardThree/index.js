import React, {Component} from 'react';
import '../cardThree/style.css';

const globe = require('../cardThree/globe.png');

class CardThree extends Component {
    render(){
        const {title, description} = this.props;
        return(
            <div>
                <div className = "cardThree">
                    <img className="imgStyleThree" src = {globe}/>
                    <div className = "containerThree">
                        <h4>{title}</h4>
                    </div>
                </div>
                <div className = "cardThreeB">
                    <div className = "containerThreeB">
                        <h4>{title}</h4>
                        <h3>{description}</h3>
                    </div>
                </div>
            </div>

        )
    }
}

export default CardThree;