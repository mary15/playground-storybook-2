import React, {Component} from "react";
import "../footer/style.css";
const tflogo = require('../footer/tflogo.png');

class MyFooter extends Component {
  render(){
    return (
      <div>
        <footer className ='myFooter'>
          <div className = "grid-container">
            <div className = "item1">
                <div className="tflogoDiv">
                  <img src = {tflogo} className = 'tflogoImg'></img>
                </div>
              </div>
            <div className = "item2">2</div>
            <div className = "item3">3</div>
          </div>
        </footer>
      </div>
    )
  }
}

export default MyFooter;
