import React, {Component} from 'react';
import {FormGroup} from "../horizontalInputField/styles/styles"

export class HorizontalInputField extends Component {

    render(){
        const {id, label, name, onChange} = this.props;
        return(
            <FormGroup>
                <label for= {name}>{label}</label>
                <input className = 'form-control'
                       id        = {id}
                       type      = 'text'
                       name      = {name}
                       onChange  = {onChange}
                />
            </FormGroup>
        )

    }

}

export default HorizontalInputField;