import React, {Component} from "react";
import '../sampleTwo/style.css'

const p1 = require('../sampleTwo/p1.png');
const p2 = require('../sampleTwo/p2.png');
const p3 = require('../sampleTwo/p3.png');


class Architecture extends Component {
  
  render(){
    const {id, buttonText} = this.props;
    return (
      <div>         
          <div className='archRow'>

            <div className='archRowItemOne'>
                <img className='archRowImg' src = {p1}/>
                <div className='textInside'>Compliant Audit Work</div>

            </div>
            
            <div className='archRowItemTwo'> 
                <img className='archRowImg' src = {p2}/>
                <div className='textInside'>Cached Microservices</div>

            </div>
            
            <div className='archRowItemThree'>
                <img className='archRowImg' src = {p3}/>
                <div className='textInside'>Low Latency Messaging</div>
            </div>

          </div>

          <div className = 'archRowTwo'>
            
            <div className='archRowItemFour'>
                <img className='archRowImg' src = {p1}/>
                <div className='textInside'>Multi-Asset<br></br>Data Model</div>


            </div>
            
            <div className='archRowItemFive'> 
                <img className='archRowImg' src = {p2}/>
                <div className='textInside'>Elastic Scalable <br></br>Event Source<br></br>Architecture</div>
            


            </div>
            
            <div className='archRowItemSix'>
                <img className='archRowImg' src = {p3}/>
                <div className='textInside'>Compliant Cybersecurity Framework</div>
            </div>
         
          </div>


      </div>
    )
  }
}



export default Architecture;
