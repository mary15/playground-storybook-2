import React, {Component} from "react";
import '../sampleThree/style.css'

const arc = require('../sampleThree/arc.png')

class ArchitectureTwo extends React.Component{

    render(){
        return(
            <div>
                <img className='arcImg' src = {arc}/>
            </div>
        )
    }

}

export default ArchitectureTwo;