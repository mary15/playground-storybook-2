import React, {Component} from 'react';
import '../blogPost/style.css';

class BlogPost extends Component {
    render(){
        const {title, post, date} = this.props;
        return(
            <div className = 'blogPostCard'>
                <div className = 'blogPostTitle'>
                    <h2>{title}</h2>
                </div>
                <div className = 'blogPostDate'>
                    <h6>{date}</h6>
                </div>
                <div className = 'blogPostBody'>
                    <p>{post}</p>
                </div>               
            </div>
        )
    }
}

export default BlogPost;