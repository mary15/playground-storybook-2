import React, {Component} from "react";
import '../checkbox/style.css'

class MyCheckbox extends Component {

  render(){
    const {id, labelDes} = this.props;
    return (
        <div className='checkbox' >
            <input type='checkbox' id={id}></input>
    <label for={id}>{labelDes}</label>
        </div>
    )
  }
}

export default MyCheckbox;
