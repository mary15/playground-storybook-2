import React, {Component} from "react";
import "../colorButton/style.css";


class ColorButton extends Component {


  hello () {
    alert('CLICKED');
  }
  
  render(){
    const {id, buttonText} = this.props;
    return (
      <button className = "btnColor" 
              onClick={this.hello}
              id = {id
      }>{buttonText}</button>
    )
  }
}

// const colorButton = () => {
//   return <button className="btnColor">Submit</button>;
// };

export default ColorButton;
