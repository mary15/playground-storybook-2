import React, {Component} from "react";
import '../shareButton/style.css';
const facebookIcon = require('../shareButton/facebook.png');

class ShareButton extends Component {

    render(){
    const {id, buttonText} = this.props;
    return (
      <button className = "shareButton" 
              onClick={this.hello}
              id = {id}>
        
            <img src={facebookIcon} className = 'iconStyle'></img>
            <h5 className = 'buttonTextStyle'>{buttonText}</h5>   
    </button>
    )
  }
}

export default ShareButton;
