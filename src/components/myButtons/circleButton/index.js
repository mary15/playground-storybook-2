import React, {Component} from "react";
import '../circleButton/style.css';
const down = require('../circleButton/down.png');

class CircleButton extends Component {

  render(){
    const {id} = this.props;
    return (
      <button className = "circleButton" id = {id}><div className="downImage"><img src = {down}/></div></button>
    )
  }
}

export default CircleButton;
