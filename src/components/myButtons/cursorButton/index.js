import React, {Component} from "react";
import "../cursorButton/style.css";

class CursorAnimationButton extends Component {

  render(){
    const {id, buttonText, disabled} = this.props;
    return(
    <button className = "btnCursor"
            id        = {id}
            disabled = {disabled}
    ><span>{buttonText}</span></button>
    )
  }

}

// const cursorAnimationButton = () => {
//   return (
//     <button className="btnCursor">
//       <span>Submit</span>
//     </button>
//   );
// };

export default CursorAnimationButton;
