import React, {Component} from "react";
import '../carousel/style.css';

const left = require('../carousel/images/left.svg');
const right = require('../carousel/images/right.svg');
const imageThree = require('../carousel/images/three.jpg');
const imageFour = require('../carousel/images/four.png');
const imageFive = require('../carousel/images/five.jpg');


class Carousel extends Component {

    render(){

        const track = document.querySelector('.carouse_track');
        const slides = Array.from(track.children);
        console.log(track);
        //insert js functions here        

        return (
            <div>

                <div className='carousel'>
                    <button className='carousel_button carousel_button--left' >
                        <img src={left} alt=''></img>
                    </button>
                    <div className='carousel_track-container'>
                        <ul className='carousel_track'>
                            <li className='carousel_slide'>
                                <img className='carousel_image' src={imageThree} alt=''></img>
                            </li>
                            <li className='carousel_slide'>
                                <img className='carousel_image' src={imageFour} alt=''></img>
                            </li>
                            <li className='carousel_slide'>
                                <img className='carousel_image' src={imageFive} alt=''></img>
                            </li>
                        </ul>
                    </div>
                    <button className='carousel_button carousel_button--right'>
                        <img src={right} alt=''></img>
                    </button>

                    <div className='carousel_nav'>
                        <button className='carousel_indicator current-slide'></button>
                        <button className='carousel_indicator'></button>
                        <button className='carousel_indicator'></button>
                    </div>


                </div>

            </div>
        )
    }
}

export default Carousel;
