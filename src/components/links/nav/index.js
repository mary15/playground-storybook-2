import React, {Component} from 'react';
import '../nav/style.css';

export class NavLink extends Component {

    render(){
        const {address, text} = this.props;
        return(
            <div>
                <p><b><a classname='link' href={address}>{text}</a></b></p>
            </div>
        )

    }

}

export default NavLink;