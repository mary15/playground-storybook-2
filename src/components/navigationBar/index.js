import React, {Component} from 'react';
import '../navigationBar/style.css';

class NavigationBar extends Component {
    
    render(){
        return (
            <div>
                <ul className='ul'>
                    <li ><a class="active" href="#home">HOME</a></li>
                    <li><a href="#news">WHAT WE DO</a></li>
                    <li><a href="#contact">SOLUTIONS</a></li>
                    <li><a href="#about">OUR BLOG</a></li>
                    <li><a href="#contactus">CONTACT US</a></li>

                </ul>
            </div>
        )
    }

}

export default NavigationBar;