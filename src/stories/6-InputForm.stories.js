import React from 'react';
import { storiesOf } from '@storybook/react';
import InputFormOne from '../components/contactUs/inputFormOne/index'
import InputFormTwo from '../components/contactUs/inputFormTwo/index'
import TextAreaOne from '../components/contactUs/textArea/index'

storiesOf("Input Form One", module)
.add("Input form One", () => (<InputFormOne id = 'inputForm'
                                            placeholder = 'First Name'/>))
.add("Input form Two", () => (<InputFormTwo id = 'inputForm'
                                            placeholder = 'First Name'/>))
.add("Text Area", () => (<TextAreaOne id = 'textArea'
                                            placeholder = 'message...'/>))