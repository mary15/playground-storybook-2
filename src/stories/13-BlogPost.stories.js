import React from 'react';
import { storiesOf } from '@storybook/react';
import BlogPost from '../components/blogPost/index';
import { withKnobs, text} from '@storybook/addon-knobs';

const stories = storiesOf('Blog Post', module);

stories.addDecorator(withKnobs);

stories.add("Post", () => (<BlogPost 
                            title = {text('label', 'Blog post title')}
                            date = {text('date', 'January 14, 2019')}
                            post = {text('post', 'insert blog post here')}/>));
