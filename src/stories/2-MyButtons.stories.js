import React from "react";
import { storiesOf } from "@storybook/react";

import CursorAnimationButton from "../components/myButtons/cursorButton/index";
import ColorButton from "../components/myButtons/colorButton/index";
import CircleButton from '../components/myButtons/circleButton/index';
import TestButton from '../components/myButtons/testButton/index';
import ShareButton from '../components/myButtons/shareButton/index';

import { withKnobs, text, boolean } from '@storybook/addon-knobs';

export default {
  title: "My Buttons"
};

storiesOf("My Button", module).addDecorator(withKnobs)
  .add("Cursor Button", () => <CursorAnimationButton  disabled={boolean('Disabled', false)}
                                                      buttonText = {text('Label', 'BACK TO HOMEPAGE')}></CursorAnimationButton>)
  .add("Color Button", () => <ColorButton id          = {text('id', 'colorBtn')}
                                          buttonText  = {text('label', 'Submit')}/>)
  .add("Circle Button", () => <CircleButton id        = 'circleButton'/>)
  .add("Test Button", () => <TestButton/>)
  .add("Share Button", () => <ShareButton id          = {text('id', 'shareButton')}
                                          buttonText  = {text('label', 'Facebook')}/>);
  