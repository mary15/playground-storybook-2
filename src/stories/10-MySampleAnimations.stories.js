import React from 'react';
import { storiesOf } from '@storybook/react';

import SampleAnimations from '../components/sampleAnimations/sampleOne/index'
import Architecture from '../components/sampleAnimations/sampleTwo/index'
import ArchitectureTwo from '../components/sampleAnimations/sampleThree/index'

storiesOf("Sample Animations", module)
.add("Sample One", () => (<SampleAnimations />))
.add("Sample Two", () => (<Architecture />))
.add("Sample Three", () => (<ArchitectureTwo />))
