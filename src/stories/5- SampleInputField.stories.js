import React from 'react';
import { storiesOf } from '@storybook/react';
import {Wrapper, Form} from '../components/horizontalInputField/styles/styles'

import HorizontalInputField from "../components/horizontalInputField/index"

const noop = () => {};

storiesOf("horizontal input field", module).add("sample input field", () => (
    <Wrapper>
        <Form>
            <HorizontalInputField id       = "email-input-field"
                                  name     = "email"
                                  label    = "Email: "
                                  onChange = {noop}

            />
        </Form>
    </Wrapper>
))
