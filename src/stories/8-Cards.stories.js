import React from 'react';
import { storiesOf } from '@storybook/react';
import CardOne from '../components/cards/cardOne/index'
import CardTwo from '../components/cards/cardTwo/index'
import CardThree from '../components/cards/cardThree/index'

storiesOf("Cards", module)
.add("Card One", () => (<CardOne title = 'Marketplace and Blockchain'
                        description = 'Lorem ipsum Neque porro quisquam est qui dolorem ipsum quia dolor sit amet'/>))
.add("Card Two", () => (<CardTwo title = 'Traditional Exchanges'
                        description = 'lorem ipsum'/>))
.add("Card Three", () => (<CardThree title = 'Global Exchanges'
                                     description = 'Lorem ipsum Neque porro quisquam est qui dolorem ipsum quia dolor sit amet'/>))
