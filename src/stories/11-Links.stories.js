import React from 'react';
import { storiesOf } from '@storybook/react';
import NavLink from '../components/links/nav/index'

storiesOf("Links", module).add("Navigation Bar", () => (<NavLink address='https://google.com' text='Home'/>))
