import React from 'react';
import { storiesOf } from '@storybook/react';
import MyCheckbox from '../components/checkbox/index'


storiesOf("My Checkbox", module)
.add("Checkbox", () => (<MyCheckbox id='checkbox_1'
                                    labelDes = 'I agree to the terms of service of this site.' />))
