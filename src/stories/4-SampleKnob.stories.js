import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';

const stories = storiesOf('Storybook Knobs', module);

stories.addDecorator(withKnobs);

stories.add('with a button', () => (
    <button disabled={boolean('Disabled', false)} >
      {text('Label', 'Sample with knob')}
    </button>
  ));

stories.add('sample with knob', () => {
    const name = text('Name', 'Mary Amora');
    const age = number('Age', 21);
    
    const content = `I am ${name} and I am ${age} years old.`;
return (<div>{content}</div>);
});