import React from "react";
import { storiesOf } from "@storybook/react";
import MyFooter from "../components/footer/index";

export default {
  title: "Footer"
};


storiesOf("Footer", module).add("Footer", () => <MyFooter />);
